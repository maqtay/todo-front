module.exports = {
    'app should work': (browser) => {
        const devServer = browser.globals.devServerURL

        browser
            .url(devServer)
            .waitForElementVisible('#app', 5000)
            .pause(4000)
            .end()
    }
}