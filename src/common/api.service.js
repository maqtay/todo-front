import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import API_URL from "./config";

const ApiService = {
    init() {
        Vue.use(VueAxios, axios);
        Vue.axios.defaults.baseURL = API_URL;
    },

    post(resource, params) {
        return Vue.axios.post(`${resource}`, params)
    },

    async get(resource) {
        var response = await Vue.axios.get(`${resource}`)
        return response.data
    },

    async delete(resource, todoID) {
        return Vue.axios.delete(`${resource}`, {params:  {id: todoID}})
    }
};

export default ApiService;

export const ToDoService = {
    create(todoText) {
        var data = new FormData()
        data.append('todo', todoText)
        ApiService.post("addtodo", data)
        .then((response) => {
            return response;
        });
    },
    async fetchToDo() {
        return ApiService.get("getalltodo");
    },
    async deleteTodo(todoID) {
        return ApiService.delete("deletetodo", todoID)
    }
};
