import { ToDoService } from "@/common/api.service";
import { ADD_NEW_TODO, DELETE_TODO, FETCH_ALL_TODO } from "./acitons.type";


const actions = {
    async [FETCH_ALL_TODO] () {
        const data = await ToDoService.fetchToDo()
        return data;
    },
    [ADD_NEW_TODO](context, {note}) {
        ToDoService.create(note)
    },
    async [DELETE_TODO](context, {id}) {
        ToDoService.deleteTodo(id)
    }
}
export default {actions}