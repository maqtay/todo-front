/* eslint-disable no-undef */
const { mount } = require("@vue/test-utils")
import ToDoList from './ToDoList.vue'

describe('ToDoList.vue', () => {
    const wrapper = mount(ToDoList)
    const mockDataTodo = [
        {
            'id': 1,
            'note': 'Hi Guys',
        },
        {
            'id': 2,
            'note': 'say hello',
        }
    ] 
    it('should todolist preview', () =>{
        wrapper.setData({todos: mockDataTodo})
        expect(wrapper.vm.todos).toBe(mockDataTodo)
    })

    it('TodoList Addtodo function testing', () => {
        wrapper.vm.addToDo("Hi Guys")
        expect(wrapper.vm.todos[0].note).toBe("Hi Guys")
    })

    it('ToDoList fetchalldata method testing', () => {
        const testFetch = jest.spyOn(ToDoList.methods, 'fetchAllData')
        mount(ToDoList)
        expect(testFetch).toHaveBeenCalled()
    })

})
