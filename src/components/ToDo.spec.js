/* eslint-disable no-undef */
const {mount} = require("@vue/test-utils")
import ToDo from './ToDo.vue'

describe('ToDo.vue', () => {
    it('should ToDo preview', () => {
        const vm = mount(ToDo ,{
            propsData: {
                todoText: "Hi Guys",
                id: 1
            },
        })

        expect(vm.props('todoText')).toBe("Hi Guys")
        expect(vm.props('id')).toBe(1)
    })
})
