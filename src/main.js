import Vue from 'vue'
import App from './App.vue'
import store from './store'

import ApiService from "./common/api.service";
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

ApiService.init()

new Vue({
  store,
  render: h => h(App),
}).$mount('#app')
