# Vuejs ToDo App

Bu uygulama Vuejs ile yazılmış bir todo application örneğidir. Golang echo ile yazılan bir backend sunucusuna bağlıdır.

Canlı link: http://159.89.10.62/

## Kullanılan Teknolojiler
- Vuejs
- Jest

## Amacı
ToDo eklemek, silmek ve listelemek

## Kurmak için yapılması gerekenler
```
$ yarn
$ docker build -t maktay/ todofront .
$ docker run maktay/todofront
```

Backend sunucusu ayaktaysa bağlantı kurulabilir. (Backend projesi)[https://gitlab.com/maqtay/todo-back]

## Deployment

```
$ docker stack deploy --compose-file docker-compose.yml todo-app
```
